#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"

#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>

#include <omp.h>

#define FACTOR_R 0.2126
#define FACTOR_G 0.7152
#define FACTOR_B 0.0722

void grayscale(struct img_t *img, struct img_t *result)
{
	int index = 0; 
	for(int i = 0; i < img->height; i++) {
		for(int j = 0;  j  < img->width; j++) {
			result->data[i * img->width + j] = 
								FACTOR_R * img->data[index + R_OFFSET] +
								FACTOR_G * img->data[index + G_OFFSET] +
								FACTOR_B * img->data[index + B_OFFSET]; 
		index += img->components;														
		}
	}
}

void invert_colors(struct img_t *img, struct img_t *result)
{
	__m128i data_val;
	__m128i data_sub_val = _mm_set_epi32(255, 255, 255, 255);
	
	data_val =  _mm_load_si128((__m128i*)&img->data[0]);
	unsigned loop = img->height *  img->width * img->components;
	
	#pragma omp parallel for
    for (int i = 0; i < loop; i += 16){
			data_val =  _mm_load_si128((__m128i*)&img->data[i]);
			data_val = _mm_sub_epi8(data_sub_val, data_val);
			_mm_store_si128((__m128i*)&result->data[i], data_val);        
    }
}


void constrat(struct img_t *img, struct img_t *result, float contrast_val)
{
	int tmp = 0;
	unsigned loop = img->height *  img->width * img->components;

	#pragma omp parallel for
	for (int i = 0; i < loop; i++){
		tmp = contrast_val * img->data[i];
		result->data[i] = tmp > 255 ? 255 : tmp; 
	}
}

void brightness(struct img_t *img, struct img_t *result, int val)
{	
	__m128i data_val;
	__m128i data_val_tmp;
	__m128i result_sl;
	__m128i add_val = _mm_set1_epi8(val);
	unsigned loop = img->height *  img->width * img->components;
	
	#pragma omp parallel for
    for (int i = 0; i < loop; i += 16){
		data_val_tmp =  _mm_load_si128((__m128i*)&img->data[i]);
		data_val = _mm_add_epi8(data_val_tmp, add_val);
		result_sl = _mm_cmpeq_epi8(_mm_max_epu8(data_val_tmp, data_val), data_val_tmp);
		data_val = _mm_or_si128(data_val, result_sl); 
		_mm_store_si128((__m128i*)&result->data[i], data_val);        
	}
}


void threshold(struct img_t *img, struct img_t *result, int limit)
{
	int tmp_val = 0; 
	int index = 0; 
	for(int i = 0; i < img-> height; i++) {
		for(int j = 0; j < img->width; j++) {
			tmp_val = 0.2126 * img->data[index + R_OFFSET] + 0.7152 * img->data[index + G_OFFSET] + 0.0722 * img->data[index + B_OFFSET];
			result->data[i * img->width + j] = tmp_val < limit ? 0 : 255; 
			index += img->components;
		}
	}
}

/*
void brush_smoothing(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;

    pixel matrix_aux[rows][columns];

    for(i=0; i<rows; i++){
        for(j=0; j<columns; j++){
            matrix_aux[i][j].R = matrix[i][j].R;
            matrix_aux[i][j].G = matrix[i][j].G;
            matrix_aux[i][j].B = matrix[i][j].B;
        }
    }

    for (i = 1; i < rows-1; i++){
        for (j = 1; j < columns-1; j++){
            matrix[i][j].R = (matrix_aux[i-1][j-1].R + matrix_aux[i-1][j].R + matrix_aux[i-1][j+1].R + \
                              matrix_aux[i][j-1].R   + matrix_aux[i][j].R   + matrix_aux[i][j+1].R   + \
                              matrix_aux[i+1][j-1].R + matrix_aux[i-1][j].R + matrix_aux[i-1][j+1].R)/9;

            matrix[i][j].G = (matrix_aux[i-1][j-1].G + matrix_aux[i-1][j].G + matrix_aux[i-1][j+1].G + \
                              matrix_aux[i][j-1].G   + matrix_aux[i][j].G   + matrix_aux[i][j+1].G   + \
                              matrix_aux[i+1][j-1].G + matrix_aux[i-1][j].G + matrix_aux[i-1][j+1].G)/9;

            matrix[i][j].B = (matrix_aux[i-1][j-1].B + matrix_aux[i-1][j].B + matrix_aux[i-1][j+1].B + \
                              matrix_aux[i][j-1].B   + matrix_aux[i][j].B   + matrix_aux[i][j+1].B   + \
                              matrix_aux[i+1][j-1].B + matrix_aux[i-1][j].B + matrix_aux[i-1][j+1].B)/9;
        }
    }
    fix_matrix(rows, columns, matrix);
}
*/



void sepia(struct img_t *img, struct img_t *result)
{
	int tmp_R, tmp_G, tmp_B;
	for(int i = 0; i < img->height; i++) {
		for(int j = 0; j < img->width; j++) {
			tmp_R = img->data[i * img->width * img->components + j * img->components + R_OFFSET];
			tmp_G = img->data[i * img->width * img->components + j * img->components + G_OFFSET];
			tmp_B = img->data[i * img->width * img->components + j * img->components + B_OFFSET];
			 result->data[i * img->width * img->components + j * img->components + R_OFFSET] = 0.393 * tmp_R + 0.769 * tmp_G + 0.189 * tmp_B > 255 ? 255 : 0.393 * tmp_R + 0.769 * tmp_G + 0.189 * tmp_B; 
			 result->data[i * img->width * img->components + j * img->components + G_OFFSET] = 0.349 * tmp_R + 0.686 * tmp_G + 0.168 * tmp_B > 255 ? 255 : 0.349 * tmp_R + 0.686 * tmp_G + 0.168 * tmp_B; 
			 result->data[i * img->width * img->components + j * img->components + B_OFFSET] = 0.272 * tmp_R + 0.534 * tmp_G + 0.131 * tmp_B > 255 ? 255 : 0.272 * tmp_R + 0.534 * tmp_G + 0.131 * tmp_B; 
		}
	}
}



int image_filtering( struct img_t * img, char **argv, int argc)
{

	char* effect_choice = argv[3];
	int val_effect;
	float val_effect_f;
	struct img_t *result_img;

    if(strcmp(effect_choice, "CIN") == 0){
		result_img = allocate_image(img->width, img->height, 1);
        grayscale(img, result_img);
    }
    
    else if(strcmp(effect_choice, "INV") == 0){
		result_img = allocate_image(img->width, img->height, img->components);
        invert_colors(img, result_img);
    }
    else if(strcmp(effect_choice, "LIM") == 0){
        if(argc != 5) {
			printf("usage : ./filter <input> <output> LIM <val_con>\n");
			return -1;
		}
		result_img = allocate_image(img->width, img->height, 1);
		val_effect = strtol(argv[4], NULL, 10);
        threshold(img, result_img, val_effect);
    }
	/*
    else if(strcmp(effect_choice, "MED") == 0){
        brush_smoothing(img->height, img->width, matrix);
    }
	*/
    else if(strcmp(effect_choice, "CON") == 0){
		if(argc != 5) {
			printf("usage : ./filter <input> <output> CON <val_con>\n");
			return -1;
		}
		result_img = allocate_image(img->width, img->height, img->components);
		val_effect_f = atof(argv[4]);
        constrat(img, result_img, val_effect_f);
    }
    else if(strcmp(effect_choice, "BRI") == 0){
        if(argc != 5) {
			printf("usage : ./filter <input> <output> BRI <val_con>\n");
			return -1;
		}
		result_img = allocate_image(img->width, img->height, img->components);
		val_effect = strtol(argv[4], NULL, 10);
        brightness(img, result_img, val_effect);
    }
    else if(strcmp(effect_choice, "SEP") == 0){
		result_img = allocate_image(img->width, img->height, img->components);
        sepia(img, result_img);
    }	
    else if(strcmp(effect_choice, "PERF") == 0){
		result_img = allocate_image(img->width, img->height, img->components);
        invert_colors(img, result_img);
        brightness(img, result_img, 150);
        constrat(img, result_img, 2.0);
    }		
	else {
			printf("This filter doesn't exist.\nFilters available : \n\t- CIN (grayscale) \n\t- INV (invert colors) \n\t- LIM (lines) \n\t- MED (blur) \n\t- CON (contrast)\n\t- BRI (brightness)\n\t- SEP(sepia\n");
			return 0; 
	}
	
	 //save_image(argv[2], result_img);
	 free(result_img);
	 
    return 0;
}
