# HPC projet : Amélioration d'une librairie

##### A.Gabriel Catel Torres

Note : 

- lib_base : code adapté directement depuis le code pour faire le traitement sur des images.
- lib : code adapté pour être plus général.
- lib_opti : librairie optimisée finale avec les meilleurs performances.

Le code de lib_base ne fonctionne qu'avec des images de petite taille (250x250 fournie fonctionne).

Enfin voici le projet de base : https://github.com/ahayasic/filtering-images





## Commandes pour tester

<u>Petite image :</u> 

- ./filter images/test_small.jpg images/test_small.png INV
- ./filter images/test_small.jpg images/test_small.png CON 1.8
- ./filter images/test_small.jpg images/test_small.png BRI 150

Grande image :</u> 

- ./filter images/landscape_big.jpg images/landscape_big.png INV
- ./filter images/test_small.jpg images/landscape_big.png CON 1.8
- ./filter images/test_small.jpg images/landscape_big.png BRI 150

Tous les autres filtres sont possibles mais le travail a été fait principalement sur ces trois filtres. 

Ces commandes fonctionnent pour



## Choix de la librairie

Pour commencer, je suis depuis le début de mes études particulièrement intéressé par les études sur les compressions (vidéo, audio, image...) et par les filtres existants. La façon de représenter l'information d'une image ou d'un son est une partie de l'informatique qui m'a toujours passionné.

Ainsi il était évident pour moi de faire des recherches sur une librairie qui touche à la compression, à des filtres ou bien tout simplement qui a une relation avec la vidéo, la photo ou la musique. Malheureusement le monde de la compression est incroyable mais les librairies sont justement pour la plupart déjà très bien pensées en terme de performances puisque c'est le but premier de la compression. De plus les algorithmes sont en générales assez complexes et se plonger dans ce genre de projet peut très rapidement être "time consuming" (beaucoup trop par rapport au temps alloué pour le projet).

Ainsi il reste tout de même un domaine qui m'intéresse : les filtres et comment les appliquer. De plus il n'est pas très compliqué d'avoir un résultat agréable sur lequel il est possible d'expérimenter les modifications apportées.

Après quelques recherches, j'ai décidé de me lancer sur une librairie proposant plusieurs filtres qui sont destinés à être appliqués sur des images : https://github.com/ahayasic/filtering-images



## Analyse du code

Pour commencer il est important de bien comprendre la structure du code proposé. C'est visiblement une librairie qui n'est pas prête à l'emploi. En effet le développeur de celle-ci effectue les modifications sur un tableau de structure (la structure contient 3 int : R, G, B et le tableau contient autant de structures que de pixels).

Chaque filtre est appliqué sur ce tableau et le seul test effectué pour voir si tout s'est bien passé est un print des valeurs. Il va comparer les valeurs d'entrées (qu'il fourni lui-même) et de sortie et vérifier que le filtre a bien été appliqué numériquement.



## Adaptation du code de base

De sorte à faire les tests de manière plus ludique et plus réels, je vais adapter le code pour qu'il prennent en entrée une image réelle et que la sortie soit l'image avec le filtre choisi appliqué. Je ne vais pour ce projet permettre que la modification d'images de type JPG mais il est tout a fait possible dans une version ultérieure d'adapter le bout de code pour que toutes les images soient modifiables.

Dans un premier temps je vais créer un main qui va récupérer l'image et appeler la fonction main du code de base (qui sera renommée "image_filtering"). Dans cette fonction je vais adapter l'information récupérée pour qu'elle s'adapte au code récupéré. Ce code sera considéré comme étant le code de base pour le travail. Ce dossier sera fourni en plus pour que les tests de performances soient visibles.

Malheureusement, sa méthode utilisant des tableaux, la taille de l'image maximum est vite une limite qui provoque un segfault dans le code. Je proposerai donc une version intermédiaire supplémentaire pour des tests sur des images de tailles supérieures.

Le code utilisé pour la gestion des images est celui fourni dans les laboratoires d'HPC (labo 5) que j'ai adapté pour sortir des images JPG.

Le code de base fourni n'est pas spécialement propre mais c'est une adaptation brute pour que le code fonctionne.

A partir de cette étape tous les filtres fonctionnent : 

![P1_01_adapt_result_img](./images/P1_01_adapt_result_img.png)

Les transformations ont l'air d'être correctes, la base est donc ce premier code.



## Améliorations de base

La première amélioration évidente, avant même de faire une analyse des performances, et qui permettra de surcroît de faire des analyses avec des images plus grandes, donc des analyses plus intéressantes et plus pertinentes, est de supprimer ce tableau de valeurs qui provoque des segfault si l'image est trop grande. En effet, cette structure est stockée sur la stack, et celle-ci à une taille maximale limite.

Avoir les informations d'une image dans la stack est insensé puisque les images contiennent un nombre de pixel qui est bien trop important. Cependant la stack permet d'avoir un accès aux valeurs plus rapide que si l'information est stockée sur le heap. Malheureusement, ce n'est pas un projet pour lequel il est possible d'utiliser la stack de cette façon.

Toutes les opérations se feront donc directement sur la structure dans laquelle est stockée l'image.

Sans même avoir à faire des tests sur l'efficacité de l'exécution du code, certaines fonctions modifient l'image de base pour sortir une image en noir et blanc (fonctions grayscale (CIN) et threshold (LIM)). Les calculs dans le code de base sont mis dans une variable et la même valeur est stockée trois fois dans des adresses différentes. Le but est de modifier le code de sorte à ce que l'image qui sera produite en sortie n'ait qu'un seul "component". Cela permettra de grandement réduire les accès mémoires et donc d'avoir un code bien plus efficace en plus d'éviter la redondance des informations.

Enfin la toute dernière modification plus qu'évidente est l'appel à la fonction "fix_matrix" dont le but est de gérer les valeurs de sorte à ce que 0 < valeur < UINT8_MAX. Cette fonction est constamment appelée et vérifie les deux bornes tandis que certains algorithmes ne peuvent de toute façon pas dépasser les bornes et pour les autres il n'est utile de vérifier que la borne haute ou la borne basse, mais pas les deux. C'est une opération à effectuer dans chacune des fonctions. De plus il est intéressant d'optimiser les accès aux données. Si une donnée est présente en cache, il est important d'effectuer toutes les opérations possibles tant que l'accès est donné pour optimiser le temps d'exécution du programme.

Ce code permet d'avoir un base un peu différente mais plus intéressante sur laquelle travailler, en effet il est à présent possible d'utiliser de très grandes images donc de voir l'exécution du programme sur des longs runs.



## Analyse des performances : 1ère approche

Pour les analyses, je vais prendre une image plutôt grande, ce qui permettra de vérifier les comportements de la cache et des opérations de manière plus poussée.

Il est aussi important de noter qu'il existe trois types de fonctions qui se ressemblent à améliorer. Pour commencer, les fonctions qui déterminent une valeur d'après la valeur des trois pixels RGB d'une itération. Puis les fonctions modifiant valeur par valeur les datas sans aucune dépendances et enfin les images qui sortent une image en grayscale (donc une seule valeur par pixel représentant la variation de gris).

Pour se projet, je vais me concentrer sur les filtres qu'il est le plus possible d'optimiser en terme de vitesse d'exécution, c'est-à-dire les filtres n'ayant aucunes dépendances.



### 1. Vérification des memory leaks

Dans tous les projets il est important de toujours vérifier qu'aucune fuite de mémoire n'est possible.

valgrind --tool=memcheck --leak-check=yes ./filter images/landscape_big.jpg images/landscape_INV.jpg INV

Le résultat est le suivant : 

![P1_02_memory_leaks](./images/P1_02_memory_leaks.png)

Pas de memory leaks possible à signaler sur le code modifié, ce qui est essentiel. La phase de test peut débuter sans attendre.



### 2. Approche avec "time"

Avec la commande time (en exécutant le même programme), il est intéressant de comparer les deux exécutions avant/après. 

La différence est minime et en regardant un peu avec perf, il est rapide de se rendre compte que ce qui prend le plus de temps dans l'exécution du programme est le fait de sauver cette image. 

Ce n'est pas une partie que je vais améliorer dans ce projet, ainsi, le save_image ne sera utilisé que pour vérifier les sorties du programme et confirmer que l'algorithme effectue toujours les bonnes opérations après la modification apportée. Les améliorations de performance et l'analyse seront plus facile à faire de cette façon. 

Le résultat suivant est donc plus parlant et donne une première approche de l'amélioration apportée : 

<img src="./images/P1_04_time_sans_save_diff.png" alt="P1_04_time_sans_save_diff" align="left" style="zoom:100%;" />

Le gain est assez notable pour le filtre "INV" Le but est de montrer l'amélioration des filtres "similaires". 

Cette fonction est celle qui sera la plus optimale puisqu'il suffit de faire l'opération suivante : 255 (UINT8_MAX) - valeur R/G/B du pixel. Cette opération ne peut pas avoir d'effets de bods puisque la "pire" opération sera 255 - 255 = 0. Toutes les autres valeurs seront de tout façon supérieures à 0. 

Cependant en prenant la fonction "brightness", un if permettant de déterminer si la valeur est supérieure à UINT8_MAX est présent. Il est possible de supprimer ce branchement avec des bithacks, pour déterminer quelle valeur doit être prise entre la valeur calculée et UINT8_MAX : 

Un test rapide avec time montre que malheureusement le bithack pour prendre le max donne des trésultats moins bons que le code de base. Cependant avec les SIMD, pas le choix, les intructions de branch n'existent pas...

![P1_05_time_BRI_bithack](./images/P1_05_time_BRI_bithack.png)

Voici le résultat avec à droite la version avec le bithack. Légèrement plus lent mais il faut tester avec les intructions SIMD pour voir la différence de temps d'exécution.

A noter que si la valeur de "BRI" passée est haute (100 +), les temps d'exécutions sont beaucoup plus proches. En effet si la valeur est trop haute (200+) le if ira toujours dans la prise de la valeur 255 et le branch prediction aura un rendement très intéressant, de même pour des valeurs très faibles, qui auront pour conséquences de constamment récupérer la valeur calculée. Dans un cas situé dans des valeurs du milieu, le branch aura beaucoup plus de pénalités car des erreurs de branch prediction vont survenir.

** note : Cette variabilité dépend aussi fortement de l'image de base évidemment.

Pour des valeurs pour lesquelles le branch prediction subit des pénalités, voici le résultat : 

![P1_06_bri_bithack_branchpred_penality](./images/P1_06_bri_bithack_branchpred_penality.png)

Les résultats sont bien plus proches et en modifiant pour faire les calculs avec des SIMD, les performances devraient être bosstées. Cependant, il faut absolument qu'il soit possible lors du calcul d'avoir une valeur plus grande que UINT8_MAX, 8 bits ne suffisent donc pas pour effectuer ces calculs.

Cette solution étant compliquée à réaliser de cette façon avec des SIMD, j'ai trouvé après plusieurs recherches une solution alternative plus intéressante et qui accélère le code en supprimant le branch : https://stackoverflow.com/questions/33481295/saturating-subtract-add-for-unsigned-bytes

Le code suivant en résulte : 

![P1_08_code_bri_mod](./images/P1_08_code_bri_mod.png)

Avec cette méthode sans les SIMD un temps un peu meilleur est obtenu : 

<img src="./images/P1_07_bithack_modif_bri.png" alt="P1_07_bithack_modif_bri" align="left" style="zoom:100%;" />

Enfin en adaptant avec la méthode SIMD, le temps est encore plus réduit et les performances sont nettement meilleures : 

<img src="./images/P1_09_code_simd_bri.png" alt="P1_09_code_simd_bri" align="left" style="zoom:100%;" />

Pour effectuer la comparaison avec des instructions simd, il existe deux solutions : 

- https://www.informit.com/articles/article.aspx?p=1959565&seqNum=12 Cette solution qui permet de faire une comparaison avec un calcul (utilisée dans le cas juste au-dessus).
- Se débrouiller pour avoir un x<y sur des unsigned avec des instructions SIMD (les instructions de bases permettent des comparaisons avec des signed sur 8 bits). https://stackoverflow.com/questions/32945410/sse2-intrinsics-comparing-unsigned-integers J'ai utilisé cette solution en utilisant la méthode y >= x qui correspond à x < y.

Les performances se trouvent encore améliorées avec cette méthode. L'avantage de cette méthode est que le temps d'exécution ne varie absolument pas suivant l'image et la valeur passée. L'exécution est plus rapide et plus prédictible, ce qui est très positif pour un programme.

Je ne peux malheureusement pas montrer un screen de la différence entre ce code avec SIMD et sans car les tests d'exécutions ont été fait à des moments différents et le système n'est pas chargé de la même façon. Le temps de base est plus long, tout comme le nouveau temps obtenu, mais la différence montre l'amélioration : 

![P1_10_code_simd_bri_amelio](/home/gap/labos/hpc/hpc20_student/hpc_project_lib_opti/report/images/P1_10_code_simd_bri_amelio.png)

Le gain est réel ! 



### 3. Analyse avec l'outil PERF

sudo perf stat ./filter images/landscape_big.jpg images/landscape_INV.jpg INV

Cette commande permet d'avoir un aperçu globale de l'exécution : 

![P2_01_perf_stat](./images/P2_01_perf_stat.png)

La différence entre le programme de base est assez flagrante. Ce programme de test va exécuter les unes après les autres les filtres suivant : INV, CON et BRI. les trois fonctions optimisées devraient donner des statistiques plus intéressantes.

Analyse de la sortie : 

- Gros gain sur le temps : 621.38 pour 359.63 pour l'amélioré. 
- Moins de context switches (surement du au temps d'exécution donc pas à prendre en compte)
- Beaucoup moins de cycles et beaucoup moins d'instructions (effet SIMD) : ratio 1/2. 
- Une perte des instructions/cycles est à noter, mais reste assez élevé et cette légère différence est très largement compensée.
- Grosse réduction des "branch" (optimisation des SIMD). Donc beaucoup moins de branch miss.

Le temps d'exécution est fortement réduit, l'optimisation est donc un succès.



Pour confirmer cette analyse, il est possible d'aller voir plus en détail ce qui prend du temps dans l'exécution avec perf record/report : ![P2_02_perf_cpu_clocks](./images/P2_02_perf_cpu_clocks.png)

En analysant les cpu-clocks dans perf, la diminution est nette surtout pour les fonctions brightness et invert_colors (SIMD utilisées), mais évidemment moins pour contrast.

![P2_03_perf_branch_miss](./images/P2_03_perf_branch_miss.png)

Même phénomène pour les branch miss évidemment puisque les if ont étés éliminés, remplacés par des calculs.

Les caches miss sont les mêmes pour les deux programmes. En effet ils sont inévitables et sont déjà assez optimisés de par l'accès correct aux données.

![P2_04_perf_intr](./images/P2_04_perf_intr.png)

Les instructions évidemment tout comme les cpu-clocks, montrent un changement similaire.





### 4. Analyse avec l'outil Valgrind

D'après toutes les analyses précédentes, l'utilisation de l'outil Valgrind n'a pas vraiment de sens puisque la cache d'un programme à l'autre n'a pas l'air de se comporter différemment.

Malgré tout il est intéressant de confirmer cela avec l'utilisation de l'outil.

![P3_valgrind_show](./images/P3_valgrind_show.png)

Les informations récupérées par Valgrind ne sont pas surprenantes et correspondent avec l'analyse fait au préalable. En effet les seuls données qui changent vraiment sont les "refs" mais les misses restent les mêmes.

Il est peut-être possible de faire des modifications pour gérer la cache un peu mieux mais l'amélioration que j'ai fait ne propose pas cela. Ce sera pour une V2 de l'amélioration de cette librairie.



### 5. Parallélisation du programme

Enfin il est possible d'améliorer les performances encore plus en parallélisant les calculs différent processeurs. Pour cela il est possible de le faire à la main avec pthread mais il existe la librairie openmp qui permet de le faire rapidement et plus efficacement. Pour cela en ajoutant juste l'include, l'option de compilation -fopenmp dans le makefile et le pragma suivant "#pragma omp parallel for", on peut de nouveau constater un léger gain de performances : 

<img src="./images/P1_11_parallelisation.png" alt="P1_11_parallelisation" align="left" style="zoom:100%;" />

Ce gain peut être variable. Ce n'est pas forcément une opti des plus intéressantes car les transformations à appliquer sont peu nombreuses, ce qui implique que l'overhead de la parallélisation n'est pas forcément rentabilisé à chaque fois suivant l'état du système.

Enfin pour la fonction de contraste malheureusement il est compliqué d'utiliser les SIMD efficacement car le calcul consiste à multiplier une valeur de 8 bits avec un float (32 bits). Les conversions à effectuer prennent du temps et ce temps n'est clairement pas intéressant. 

Malgré tout, la parallélisation reste intéressante. En effet la différence de temps d'exécution se ressent encore plus que sur les fonctions précédentes puisqu'aucune optimisation avec les SIMD n'a été faite : 

![P1_12_para_contrast](./images/P1_12_para_contrast.png)



Enfin en testant les trois fonctions d'affilé, le résultat est clairement meilleur : 

![P4_01_parall](./images/P4_01_parall.png)

Le temps réel d'exécution est bien inférieur, l'overhead de la création des threads est largement compensé.

Dans l'absolue pour améliorer les autres fonctions, il faudrait retoucher le code pour paralléliser au mieux car les SIMD ne s'y prettent clairement pas. Les performances seraient de toute façon bien améliorées.	



## Conclusion

Ce projet a été une bonne expérience mais avec du recul j'aurais choisis une librairie différente, déjà prête à l'emploi pour éviter ce temps d'adaptation pour utiliser le code dans une situation réelle.

Une librairie officielle fourni de cette manière ne fonctionnerait évidemment pas, le but était surtout de pratiquer les outils et de voir sur un exercice plus réel comment améliorer les performances d'exécution d'un code. Pour produire une librairie utilisable, il faut un code basique qui fonctionne et utiliser les options (SIMD ou parallélisation) uniquement si c'est possible pour un système.

J'ai aussi pu remarquer que la sauvegarde d'image était clairement ce qui prenait le plus de temps, il serait intéressant de voir s'il est possible d'améliorer ou pas cette librairie, mais pas dans ce projet.



