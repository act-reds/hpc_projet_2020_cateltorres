#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"

typedef struct{
  int R, G, B;
} pixel;

void input_matrix(int rows, int columns, pixel matrix[rows][columns], struct img_t* img){
    int i, j;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++) {
            matrix[i][j].R = img->data[i * img->width*3 + j*3 + R_OFFSET];
			matrix[i][j].G = img->data[i * img->width*3 + j*3 + G_OFFSET];
			matrix[i][j].B = img->data[i * img->width*3 + j*3 + B_OFFSET];
		}
    }
}

void print_matrix(int rows, int columns, pixel matrix[rows][columns]) {
    printf("%d %d\n", rows, columns);
    int i, j;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++)
            printf("%d %d %d ", matrix[i][j].R, matrix[i][j].G, matrix[i][j].B);
    }
}

void fix_matrix(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            if(matrix[i][j].R < 0) matrix[i][j].R = 0;
            else if(matrix[i][j].R > 255) matrix[i][j].R = 255;

            if(matrix[i][j].G < 0) matrix[i][j].G = 0;
            else if(matrix[i][j].G > 255) matrix[i][j].G = 255;

            if(matrix[i][j].B < 0) matrix[i][j].B = 0;
            else if(matrix[i][j].B > 255) matrix[i][j].B = 255;
        }
    }
}

void grayscale(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;
    int g;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            g = (0.2126*matrix[i][j].R + 0.7152*matrix[i][j].G + 0.0722*matrix[i][j].B);
            matrix[i][j].R = g;
            matrix[i][j].G = g;
            matrix[i][j].B = g;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void invert_colors(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            matrix[i][j].R = 255-matrix[i][j].R;
            matrix[i][j].G = 255-matrix[i][j].G;
            matrix[i][j].B = 255-matrix[i][j].B;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void threshold(int rows, int columns, pixel matrix[rows][columns], int l){
    int i, j;
    int g_aux, g;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            g_aux = 0.2126*matrix[i][j].R + 0.7152*matrix[i][j].G + 0.0722*matrix[i][j].B;
            g = ((g_aux < l) ? 0 : 255);
            matrix[i][j].R = g;
            matrix[i][j].G = g;
            matrix[i][j].B = g;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void brush_smoothing(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;

    pixel matrix_aux[rows][columns];

    for(i=0; i<rows; i++){
        for(j=0; j<columns; j++){
            matrix_aux[i][j].R = matrix[i][j].R;
            matrix_aux[i][j].G = matrix[i][j].G;
            matrix_aux[i][j].B = matrix[i][j].B;
        }
    }

    for (i = 1; i < rows-1; i++){
        for (j = 1; j < columns-1; j++){
            matrix[i][j].R = (matrix_aux[i-1][j-1].R + matrix_aux[i-1][j].R + matrix_aux[i-1][j+1].R + \
                              matrix_aux[i][j-1].R   + matrix_aux[i][j].R   + matrix_aux[i][j+1].R   + \
                              matrix_aux[i+1][j-1].R + matrix_aux[i-1][j].R + matrix_aux[i-1][j+1].R)/9;

            matrix[i][j].G = (matrix_aux[i-1][j-1].G + matrix_aux[i-1][j].G + matrix_aux[i-1][j+1].G + \
                              matrix_aux[i][j-1].G   + matrix_aux[i][j].G   + matrix_aux[i][j+1].G   + \
                              matrix_aux[i+1][j-1].G + matrix_aux[i-1][j].G + matrix_aux[i-1][j+1].G)/9;

            matrix[i][j].B = (matrix_aux[i-1][j-1].B + matrix_aux[i-1][j].B + matrix_aux[i-1][j+1].B + \
                              matrix_aux[i][j-1].B   + matrix_aux[i][j].B   + matrix_aux[i][j+1].B   + \
                              matrix_aux[i+1][j-1].B + matrix_aux[i-1][j].B + matrix_aux[i-1][j+1].B)/9;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void constrat(int rows, int columns, pixel matrix[rows][columns], float g){
    int i, j;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            matrix[i][j].R = g * matrix[i][j].R;
            matrix[i][j].G = g * matrix[i][j].G;
            matrix[i][j].B = g * matrix[i][j].B;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void brightness(int rows, int columns, pixel matrix[rows][columns], int g){
    int i, j;
    for (i = 0; i < rows; i++) {
        for (j = 0; j < columns; j++){
            matrix[i][j].R = matrix[i][j].R + g;
            matrix[i][j].G = matrix[i][j].G + g;
            matrix[i][j].B = matrix[i][j].B + g;
        }
    }
    fix_matrix(rows, columns, matrix);
}

void sepia(int rows, int columns, pixel matrix[rows][columns]){
    int i, j;
    int pR_aux, pG_aux, pB_aux;
    for (i = 0; i < rows; i++){
        for (j = 0; j < columns; j++){
            pR_aux = matrix[i][j].R;
            pG_aux = matrix[i][j].G;
            pB_aux = matrix[i][j].B;
            matrix[i][j].R = 0.393*pR_aux + 0.769*pG_aux + 0.189*pB_aux;
            matrix[i][j].G = 0.349*pR_aux + 0.686*pG_aux + 0.168*pB_aux;
            matrix[i][j].B = 0.272*pR_aux + 0.534*pG_aux + 0.131*pB_aux;
        }
    }
    fix_matrix(rows, columns, matrix);
}


	struct img_t* image_filtering( struct img_t * img, char **argv, int argc){

    pixel matrix[img->height][img->width];
	char* effect_choice = argv[3];
	int val_effect;
	
	struct img_t *result_img = allocate_image(img->width, img->height, img->components);
	
	//printf("Wid/th = %d \nHeignt = %d \nComponents = %d\n",  img->width, img->height, img->components);

	 input_matrix(img->height, img->width, matrix, img);
	 

    if(strcmp(effect_choice, "CIN") == 0){
        grayscale(img->height, img->width, matrix);
    }
    
    else if(strcmp(effect_choice, "INV") == 0){
        invert_colors(img->height, img->width, matrix);
    }
    else if(strcmp(effect_choice, "LIM") == 0){
        if(argc != 5) {
			printf("usage : ./filter <input> <output> LIM <val_con>\n");
			return -1;
		}
		val_effect = strtol(argv[4], NULL, 10);
        threshold(img->height, img->width, matrix, val_effect);
    }
    else if(strcmp(effect_choice, "MED") == 0){
        brush_smoothing(img->height, img->width, matrix);
    }
    else if(strcmp(effect_choice, "CON") == 0){
		if(argc != 5) {
			printf("usage : ./filter <input> <output> CON <val_con>\n");
			return -1;
		}
		val_effect = strtol(argv[4], NULL, 10);
        //input_matrix(img->height, img->width, matrix, img);
        constrat(img->height, img->width, matrix, val_effect);
    }
    else if(strcmp(effect_choice, "BRI") == 0){
        if(argc != 5) {
			printf("usage : ./filter <input> <output> BRI <val_con>\n");
			return -1;
		}
		val_effect = strtol(argv[4], NULL, 10);
		printf("%d\n", val_effect);
        brightness(img->height, img->width, matrix, val_effect);
    }
    else if(strcmp(effect_choice, "SEP") == 0){
        //input_matrix(img->height, img->width, matrix, img);
        sepia(img->height, img->width, matrix);
    }	else {
			printf("This filter doesn't exist.\nFilters available : \n\t- CIN (grayscale) \n\t- INV (invert colors) \n\t- LIM (lines) \n\t- MED (blur) \n\t- CON (contrast)\n\t- BRI (brightness)\n\t- SEP(sepia\n");
			free_image(result_img);
			return 0; 
	}
	
	for (int i = 0; i < img->width; i++){
		for (int j = 0; j < img->height; j++){
			result_img->data[i * img->width*3 + j*3 + R_OFFSET] = matrix[i][j].R;
			result_img->data[i * img->width*3 + j*3 + G_OFFSET] = matrix[i][j].G;
			result_img->data[i * img->width*3 + j*3 + B_OFFSET] = matrix[i][j].B;
			/*
			printf("%d  |  R=%-3d  |  G=%-3d  |  B=%-3d  |  i=%d,  |  j=%-3d  |  R=%d  |  G=%d  |  B=%d\n", bubu++, result_img->data[i * img->width*3 + j*3 + R_OFFSET],
			                  result_img->data[i * img->width + j*3 + G_OFFSET], result_img->data[i * img->width*3 + j*3 + B_OFFSET], i * img->width*3, j*3,
							  i * img->width + j*3 + R_OFFSET, i * img->width*3 + j*3 + G_OFFSET, i * img->width*3 + j*3 + B_OFFSET);
							  */
		}
	}
	
	 save_image(argv[2], result_img);
	 free_image(result_img);

    return 0;
}
