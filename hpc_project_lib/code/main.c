#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "image.h"

#define EXPECTED_NB_ARGS 4

void print_usage()
{
    printf("Please provide an input image and an output image\n");
}

int main(int argc, char *argv[])
{
	struct img_t *input_img;

    if (argc < EXPECTED_NB_ARGS) {
        fprintf(stderr, "Invalid number of arguments\n");
        print_usage();
        return EXIT_FAILURE;
    }

    input_img = load_image(argv[1]);
	image_filtering(input_img, argv, argc);
    free_image(input_img);

    return EXIT_SUCCESS;
}
